#!/bin/bash

this_file_path=`readlink -f $0`
this_dir_path=`dirname $this_file_path`
project_dir_path=`dirname $this_dir_path`

echo $this_dir_path
cd $this_dir_path

sudo docker stack deploy --compose-file docker-compose.develop.yml webapi

cd -
