#!/bin/bash

# 將Django的靜態檔蒐集至static_root(settings.py配置)中
echo "Collect static files"
source /opt/webapi_env/bin/activate
mkdir -p /var/www/webapi/static
python /var/www/webapi/manage.py collectstatic --noinput

source /opt/webapi_2_env/bin/activate
mkdir -p /var/www/webapi_2/static
python /var/www/webapi_2/manage.py collectstatic --noinput

# 將apache推到前台，這樣才能保持apache運行狀態
/usr/sbin/apachectl -D FOREGROUND